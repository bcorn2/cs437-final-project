import serial
import string
import time
import json
from setup_logger import logger

class MoistureSensor:
    def __init__(moistureSensor, port, baudRate):
        moistureSensor.port = port
        moistureSensor.baudRate = baudRate
        moistureSensor.interface = serial.Serial(port, baudRate)

    def getSensorData(self):
        try:
            sensorData = self.interface.readline().decode('ascii').strip()
            logger.debug('MoistureSensor: ' + json.dumps(sensorData))
            return sensorData
        except RuntimeError as error:
            logger.error('MoistureSensor: ' + json.dumps(error.args[0]))
        except Exception as error:
            raise error
