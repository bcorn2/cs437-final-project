# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT
# Adapted from https://learn.adafruit.com/dht-humidity-sensing-on-raspberry-pi-with-gdocs-logging/python-setup

import time
import board
import adafruit_dht
import json
from setup_logger import logger

class HumiditySensor:

    def __init__(humiditySensor, pin):
        humiditySensor.pin = pin
        humiditySensor.interface = adafruit_dht.DHT11(getattr(board, pin))

    def getSensorData(self):
        try:
            # Print the values to the serial port
            temperature_c = self.interface.temperature
            temperature_f = self.f2c(temperature_c)
            humidity = self.interface.humidity
            sensorData = {
                'humidity': humidity,
                'temperature_f': temperature_f,
                'temperature_c': temperature_c
            }
            logger.debug('HumiditySensor: ' + json.dumps(sensorData))
            return sensorData
        except RuntimeError as error:
            logger.error('HumiditySensor: ' + json.dumps(error.args[0]))
        except Exception as error:
            raise error

    def cleanup(self):
        logger.debug('HumiditySensor: Cleaning up interface.')
        self.interface.exit()

    def f2c(self, celsius):
        return celsius * (9 / 5) + 32