import RPi.GPIO as GPIO
import time
from setup_logger import logger

class WaterPump:
    def __init__(waterPump, pin, mps = 18.0):
        waterPump.pin = pin
        waterPump.mps = mps
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(pin, GPIO.OUT)

    def startPump(self):
        GPIO.output(self.pin, GPIO.HIGH)

    def stopPump(self):
        GPIO.output(self.pin, GPIO.LOW)

    def dispense(self, ml):
        # calculate pump run time based on desired
        # dispense amount in ml / the calibrated ml per second
        
        runTime = ml / self.mps
        logger.debug("WaterPump: run time is " + str(runTime))
        self.startPump()
        time.sleep(runTime)
        self.stopPump()


