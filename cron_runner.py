#!/usr/bin/python3

import sys
from datetime import datetime
import pump

args = sys.argv

ml = float(args[1])

now = datetime.now()
waterPump = pump.WaterPump(17)

waterPump.dispense(ml)

print('Dispsense job run at: ' + str(now) + ' for ' + str(ml) + ' mL')