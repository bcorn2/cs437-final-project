$( document ).ready(function() {
  document.getElementById("dispense").addEventListener("click", dispense);
  document.getElementById("clear_all_schedules").addEventListener("click", clear_schedules);
  document.getElementById("create_schedule").addEventListener("click", create_schedule);

  schedules = get_schedules()
  get_sensor_data()
  setInterval(get_sensor_data, 30000);
});

function create_schedule() {
  day_of_week = $('#day_of_week').val()
  hour = $('#schedule_hour').val()
  minute = $('#schedule_minute').val()
  ml = $('#schedule_water_amount').val()

  axios.post('/create_schedule', {
    ml: ml,
    minute: minute,
    hour: hour,
    day_month: '*',
    month: '*',
    day_week: day_of_week
  })
  .then(function (response) {
    console.log(response);
    refresh_schedules()
  })
  .catch(function (error) {
    console.log(error);
  });
}

function get_schedules() {
  schedules = []
  axios.get('/schedules')
  .then(function (response) {
    schedules = response.data
    schedule_count = schedules.length

    formatted_schedules = []
    for (var i = 0; i < schedule_count; i++) {
      cron = schedules[i]
      els = cron['command'].split(' /')
      ml = get_ml_from_cron(cron['command'])
      sch = {
        cron: cron['readable'],
        ml: ml[1]
      }
      formatted_schedules.push(sch)
    }
    console.log(response);
    create_schedule_table(formatted_schedules)
    return formatted_schedules
  })
  .catch(function (error) {
    console.log(error);
  });
}

function create_schedule_table(schedules) {
  els = schedules.length
  table = document.getElementById("schedule_table").getElementsByTagName('tbody')[0];
  for (var i = 0; i < els; i++) {
    var row = table.insertRow();
    var cronCell = row.insertCell(0);
    var mlCell = row.insertCell(1);
    cronCell.innerHTML = schedules[i]['cron']
    mlCell.innerHTML = schedules[i]['ml']
  }
}

function dispense() {
  ml = $('#manual-dispense-amount').val()

  axios.post('/dispense', {
    ml: ml
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
}

function refresh_schedules () {
  $("#schedule_table tbody tr").remove();
  get_schedules()
}

function clear_schedules() {
  axios.post('/clear_schedules', {
  })
  .then(function (response) {
    console.log(response);
    refresh_schedules()
  })
  .catch(function (error) {
    console.log(error);
  });
}

function get_ml_from_cron(str) {
  const regex = /\.py (.*?)\ >/gm;

  let m;
  matches = []
  while ((m = regex.exec(str)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
          regex.lastIndex++;
      }
      
      // The result can be accessed through the `m`-variable.
      m.forEach((match, groupIndex) => {
          matches.push(match)
      });
  }
  return matches
}

function get_sensor_data() {
  axios.get('/sensor_data')
  .then(function (response) {
    data = response.data
    $("#soil_temp").text(data['soil_temp']);
    $("#soil_moisture").text(data['soil_moisture']);
    $("#humidity").text(data['humidity']);
    $("#external_temp").text(data['external_temp']);
    date = "Last updated at " + new Date().toLocaleString() + ". Next update within 30s."
    $("#last_updated").text(date);
  })
  .catch(function (error) {
    console.log(error);
  });
}