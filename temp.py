# SPDX-FileCopyrightText: 2019 Mikey Sklar for Adafruit Industries
#
# SPDX-License-Identifier: MIT
# https://learn.adafruit.com/adafruits-raspberry-pi-lesson-11-ds18b20-temperature-sensing/software

import glob
import time
from setup_logger import logger
import json

class TemperatureSensor:
    
    def __init__(tempSensor):
        tempSensor.baseDir = '/sys/bus/w1/devices/'
        tempSensor.deviceFolder = glob.glob(tempSensor.baseDir + '28*')[0]
        tempSensor.deviceFile = tempSensor.deviceFolder + '/w1_slave'

    def getSensorData(self):
        lines = self.readRawTemp()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = self.readRawTemp()
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos+2:]
            temp_c = float(temp_string) / 1000.0
            temp_f = self.c2f(temp_c)
            sensorData = {
                "temp_c": temp_c,
                "temp_f": temp_f
            }
            logger.debug('SoilTemperatureSensor: ' + json.dumps(sensorData))
            return sensorData
            
    def readRawTemp(self):
        f = open(self.deviceFile, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def c2f(self, temp_c):
        return temp_c * 9.0 / 5.0 + 32.0