import json
from click import command
from sys import platform
from flask import Flask, render_template
from flask import request, jsonify
from crontab import CronTab
from cron_descriptor import get_description, ExpressionDescriptor

if platform == "linux" or platform == "linux2":
    import humidity
    import moisture
    import temp
    import pump

    humiditySensor = humidity.HumiditySensor('D18')
    moistureSensor = moisture.MoistureSensor('/dev/ttyACM0', 9600)
    soilTempSensor = temp.TemperatureSensor()
    waterPump = pump.WaterPump(17)

app = Flask(__name__)
app.config["TEMPLATES_AUTO_RELOAD"] = True
cron = CronTab(user=True)

@app.route("/")
def home():
    return render_template('home.html')

@app.route("/schedules")
def schedules():
    schedules = []
    for job in cron:
        j = str(job)
        exp = j.split(' /')[0]
        s = {
            "command": j,
            "readable": get_description(exp)
        }
        schedules.append(s)
    return jsonify(schedules)

@app.route("/create_schedule", methods=['POST'])
def createSchedule():
    data = request.json
    ml = data['ml']
    min = data['minute']
    hour = data['hour']
    day_month = data['day_month']
    month = data['month']
    day_week = data['day_week']
    command = ''
    if platform == "linux" or platform == "linux2":
        command = "/home/pi/cs437-final-project/cron_runner.py " + str(ml) + " >> /home/pi/cs437-final-project/cron.log 2>&1"
    else:
        command = "/Users/benjcorn/Desktop/UIUC/CS437/cs437-final-project/cron_test.py " + str(ml) + " >> /Users/benjcorn/Desktop/UIUC/CS437/cs437-final-project/cron.log 2>&1"
    job = cron.new(command=command)
    job.setall(min, hour, day_month, month, day_week)
    cron.write()
    return {}

@app.route("/modify_schedule", methods=['POST'])
def modifySchedule():
    pass

@app.route("/clear_schedules", methods=['POST'])
def clearSchedules():
    cron.remove_all()
    cron.write()
    schedules = []

    for job in cron:
        schedules.append(str(job))
    return jsonify(schedules)

@app.route("/dispense", methods=['POST'])
def dispense():
    data = request.json
    ml = float(data['ml'])
    waterPump.dispense(ml)
    return jsonify(data)

@app.route("/sensor_data", methods=['GET'])
def sensor_data():
    if platform == "linux" or platform == "linux2":
        humidityData = humiditySensor.getSensorData()
        humidity = 'Loading...'
        external_temp = 'Loading...'
        if humidityData is not None:
            external_temp = str(round(humidityData['temperature_f'], 1)) + ' F'
            humidity = str(humidityData['humidity']) + '%'
        soil_temp = str(round(soilTempSensor.getSensorData()['temp_f'], 1)) + ' F'
        soil_moisture = moistureSensor.getSensorData()
        soil_moisture = str(soil_moisture).replace('%', '').strip() + '%'

        data = {
            "humidity": humidity,
            "soil_temp": soil_temp,
            "soil_moisture": soil_moisture,
            "external_temp": external_temp
        }
        return data
    else:
        data = {
            "humidity": '53%',
            "soil_temp": '75F',
            "soil_moisture": '30%',
            "external_temp": '80F'
        }
        return data
