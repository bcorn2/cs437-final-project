#!/usr/bin/python3

import sys
from datetime import datetime

args = sys.argv

# Opening a file
file1 = open('/Users/benjcorn/Desktop/UIUC/CS437/cs437-final-project/water_log.txt', 'a+')
s = args[1] + '\n'

now = datetime.now()
print('Job run at: ', now)

# Writing a string to file
file1.write(s)

# Closing file
file1.close()