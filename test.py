import humidity
import moisture
import temp
import pump
import time
from signal import signal, SIGINT
from sys import exit

humiditySensor = humidity.HumiditySensor('D18')
moistureSensor = moisture.MoistureSensor('/dev/ttyACM0', 9600)
soilTempSensor = temp.TemperatureSensor()
waterPump = pump.WaterPump(17)

def handler(signal_received, frame):
  humiditySensor.interface.exit()
  print('SIGINT or CTRL-C detected. Exiting gracefully')
  exit(0)

if __name__ == '__main__':
    signal(SIGINT, handler)

    print('Running. Press CTRL-C to exit.')
    for i in range(0,5):
      humidity = humiditySensor.getSensorData()
      moisture = moistureSensor.getSensorData()
      soilTemperature = soilTempSensor.getSensorData()

      time.sleep(1)

    waterPump.dispense(100)

    time.sleep(5)
    waterPump.dispense(20)

    time.sleep(5)
    waterPump.dispense(5)
